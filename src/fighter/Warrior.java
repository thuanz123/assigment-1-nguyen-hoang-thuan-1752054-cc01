package fighter;

public class Warrior extends Fighter {
	public Warrior(int baseHP, int wp) {
		super(baseHP, wp);
	}

	public double getCombatScore() {
		if(Utility.isPrime(Utility.Ground)) {
			return 2 * getBaseHp();
		} else {
			if(getWp() == 1) {
				return getBaseHp();
			}  

			return getBaseHp() / 10;
		} 
	}
}
