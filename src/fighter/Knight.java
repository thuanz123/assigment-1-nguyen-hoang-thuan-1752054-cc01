package fighter;

public class Knight extends Fighter {
	public Knight(int baseHP, int wp) {
		super(baseHP, wp);
	}

	public double getCombatScore() {
		if(Utility.isSquare(Utility.Ground)) {
			return 2 * getBaseHp();
		} else if(Utility.Ground == 999) {
			int a = 1, b = 1, c =  0;
			
			while(b <= 2 * getBaseHp()) {
				c = b;
				b = b + a;
				a = c;
			}
			
			return b;
		} else {
			if(getWp() == 1) {
				return getBaseHp();
			}  

			return getBaseHp() / 10;
		} 
	}
}
