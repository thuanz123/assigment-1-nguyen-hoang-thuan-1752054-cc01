package fighter;


public abstract class Fighter {
	private int mBaseHp;
	private int mWp;

	public abstract double getCombatScore();

	public Fighter(int BaseHp, int Wp) {
		mBaseHp = BaseHp;
		mWp = Wp;
	}

	public double getBaseHp() {
		return mBaseHp;
	}

	public void setBaseHp(int baseHp) {
		mBaseHp = baseHp;
	}

	public double getWp() {
		return mWp;
	}

	public void setWp(int wp) {
		mWp = wp;
	}
}
