package fighter;

public class Utility {
	public static int Ground;

	public static boolean isPrime(int num) {
		if(num < 2) {
			return false;
		} else {
			for(int i = 2; i < Math.sqrt(num); i++) {
				if(num % i == 0) {
					return false;
				}
			}

			return true;
		}
	}

	public static boolean isSquare(int num) {
		int x = (int) Math.sqrt(num);

		return x * x == num;
	}
}
